# Jenkins Slave Configuration 

## Steps 
- yum update -y
- yum install java-1.8.0-openjdk-devel -y

## Docker
- amazon-linux-extras install docker
- service docker start
  
## Maven  
- yum install git -y
- wget https://www-eu.apache.org/dist/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.zip
- unzip apache-maven-3.6.3-bin.zip
- mv apache-maven-3.6.3 maven && \
    mv maven /opt/maven
- rm apache-maven-3.6.3-bin.zip

## Gradle
- wget https://services.gradle.org/distributions/gradle-5.0-bin.zip -P /opt/
- cd /opt/
- unzip gradle-5.0-bin.zip
- rm -f gradle-5.0-bin.zip
- mv gradle-5.0 gradle
- wget https://releases.hashicorp.com/terraform/0.12.17/terraform_0.12.17_linux_amd64.zip

## Terraform
- unzip terraform_0.12.17_linux_amd64.zip
- mv terraform /usr/local/bin/
- rm -f terraform_0.12.17_linux_amd64.zip
- wget https://releases.hashicorp.com/packer/1.5.5/packer_1.5.5_linux_amd64.zip

## NodeJs
curl --silent --location https://rpm.nodesource.com/setup_10.x | bash - && \
    yum install nodejs -y