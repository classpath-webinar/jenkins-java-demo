package com.glearning.build;


import org.junit.Assert;
import org.junit.Test;

public class HelloWorldTest {

 @Test
 public void testSum(){
     HelloWorld helloWorld = new HelloWorld();
     int result = helloWorld.sum(45, 56);
     Assert.assertEquals(101, result);
 }
 @Test
 public void testSumNew(){
     HelloWorld helloWorld = new HelloWorld();
     int result = helloWorld.sum(40, 50);
     Assert.assertEquals(90, result);
 }

 @Test
 public void testSumUpdated(){
     HelloWorld helloWorld = new HelloWorld();
     int result = helloWorld.sum(50, 50);
     Assert.assertEquals(100, result);
 }

 @Test
 public void testSumInvalid(){
     HelloWorld helloWorld = new HelloWorld();
     int result = helloWorld.sum(40, 50);
     Assert.assertEquals(90, result);
 }

 @Test
 public void testProduct(){
     HelloWorld helloWorld = new HelloWorld();
     int result = helloWorld.product(40, 50);
      Assert.assertEquals(2000, result);
 }

}